{
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
}
unit OracleDataPumpMain;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.Menus,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  ShellApi,
  About,
  Data.DbxSqlite,
  Data.FMTBcd,
  Data.DB,
  Data.SqlExpr,
  ClipBrd;

type
  TForm1 = class(TForm)
    GPAllgemein: TGroupBox;
    btExit: TButton;
    Label1: TLabel;
    Label2: TLabel;
    edName: TEdit;
    edPassword: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edDatabase: TEdit;
    edSchema: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    edDumpfile: TEdit;
    edDir: TEdit;
    Label7: TLabel;
    UpDown1: TUpDown;
    edParJobs: TEdit;
    GBVoreinstellungen: TGroupBox;
    ComboBox1: TComboBox;
    btSave: TButton;
    GPImport: TGroupBox;
    Label8: TLabel;
    edRemapSchema: TEdit;
    edRemapTablespace: TEdit;
    Label9: TLabel;
    btExport: TButton;
    btImport: TButton;
    btInfo: TButton;
    GPExport: TGroupBox;
    cbOverwrite: TCheckBox;
    btLoad: TButton;
    SQLConnection1: TSQLConnection;
    DSInsertConnection: TSQLDataSet;
    DSSelectConnections: TSQLDataSet;
    DSSelectConfig: TSQLDataSet;
    DSUpdateConnection: TSQLDataSet;
    DSDeleteConnection: TSQLDataSet;
    btDelete: TButton;
    Label10: TLabel;
    edTableExp: TEdit;
    Label11: TLabel;
    edExpExclude: TEdit;
    Label12: TLabel;
    edTableImp: TEdit;
    Label13: TLabel;
    edImpExclude: TEdit;
    btExpClipboard: TButton;
    btImpClipboard: TButton;
    Label14: TLabel;
    edLogfile: TEdit;
    cbNoLogfile: TCheckBox;
    procedure btExitClick(Sender: TObject);
    procedure btInfoClick(Sender: TObject);
    procedure btExportClick(Sender: TObject);
    procedure btImportClick(Sender: TObject);
    procedure btSaveClick(Sender: TObject);
    procedure btLoadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btDeleteClick(Sender: TObject);
    procedure btImpClipboardClick(Sender: TObject);
    procedure btExpClipboardClick(Sender: TObject);
    procedure cbNoLogfileClick(Sender: TObject);
  private
    { Private-Deklarationen }
    function CheckParameter():boolean;
    function BuildImportString():string;
    function BuildExportString():string;
    procedure DPExImport(param, mode: string);
    procedure DPExImportToClipboard(param, mode: string);
    procedure ConnectToDatabase();
    procedure CreateTable();
    procedure LoadConfigFromDatabase();
    procedure SaveIntoDB();
    procedure UpdateDB();
    procedure DisableConfig();
  public
    { Public-Deklarationen }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btDeleteClick(Sender: TObject);
var
  FSQDeleteCommand: string;
begin
  try
    DSDeleteConnection.Close;
    FSQDeleteCommand := 'DELETE FROM StoredConnections where ConName=:name;';
    DSDeleteConnection.CommandText := FSQDeleteCommand;
    DSDeleteConnection.ParamByName('name').Value := ComboBox1.Text;
    DSDeleteConnection.ExecSQL;
    DSDeleteConnection.Close();
    ComboBox1.Items.Clear();
    ComboBox1.Text := '';
    LoadConfigFromDatabase();
  except
    on E: EDatabaseError do
        ShowMessage('Exception raised with message' + E.Message);
  end;
end;

procedure TForm1.btExitClick(Sender: TObject);
begin
  halt;
end;

procedure TForm1.btSaveClick(Sender: TObject);
var
  FSQLSelectCommand: string;
begin
  try
    DSSelectConfig.Close;
    FSQLSelectCommand := 'SELECT ConName FROM StoredConnections where ConName=:name;';
    DSSelectConfig.CommandText := FSQLSelectCommand;
    DSSelectConfig.ParamByName('name').Value := ComboBox1.Text;
    DSSelectConfig.Open;
    DSSelectConfig.Refresh;
    if DSSelectConfig.IsEmpty then
    begin
      SaveIntoDB();
    end
    else
    begin
      UpdateDB();
    end;
    DSSelectConfig.Close;
  except
    on E: EDatabaseError do
        ShowMessage('Exception raised with message' + E.Message);
  end;
end;

procedure TForm1.SaveIntoDB();
var
  mySQL: string;
begin
  if ComboBox1.Text <> '' then
  begin
    mySql := 'INSERT INTO StoredConnections (ConName, Username, Password,' +
             ' Database, Schemas, Dumpfile, Directory, ParJobs, ReuseDumpfile, ' +
             ' RemapSchema, RemapTablespace, TableExport, TableExportExclude, ' +
             ' TableImport, TableImportExclude, Logfile, NoLogfile) VALUES (:ConName, :Username,' +
             ' :Password, :Database, :Schemas, :Dumpfile, :Directory, :ParJobs,' +
             ' :ReuseDumpfile, :RemapSchema, :RemapTablespace,' +
             ' :TableExport, :TableExportExclude, :TableImport, :TableImportExclude,' +
             ' :Logfile, :NoLogfile);';
    DSInsertConnection.CommandText := mySql;
    DSInsertConnection.ParamByName('ConName').Value := ComboBox1.Text;
    DSInsertConnection.ParamByName('Username').Value := edName.Text;
    DSInsertConnection.ParamByName('Password').Value := edPassword.Text;
    DSInsertConnection.ParamByName('Database').Value := edDatabase.Text;
    DSInsertConnection.ParamByName('Schemas').Value := edSchema.Text;
    DSInsertConnection.ParamByName('Dumpfile').Value := edDumpfile.Text;
    DSInsertConnection.ParamByName('Directory').Value := edDir.Text;
    DSInsertConnection.ParamByName('ParJobs').Value := StrToInt(edParJobs.Text);
    if cbOverwrite.Checked then
    begin
      DSInsertConnection.ParamByName('ReuseDumpfile').Value := true;
    end
    else
    begin
      DSInsertConnection.ParamByName('ReuseDumpfile').Value := false;
    end;
    DSInsertConnection.ParamByName('RemapSchema').Value := edRemapSchema.Text;
    DSInsertConnection.ParamByName('RemapTablespace').Value := edRemapTablespace.Text;
    DSInsertConnection.ParamByName('TableExport').Value := edTableExp.Text;
    DSInsertConnection.ParamByName('TableExportExclude').Value := edExpExclude.Text;
    DSInsertConnection.ParamByName('TableImport').Value := edTableImp.Text;
    DSInsertConnection.ParamByName('TableImportExclude').Value := edImpExclude.Text;
    DSInsertConnection.ParamByName('Logfile').Value := edLogfile.Text;
    if cbNoLogfile.Checked then
    begin
      DSInsertConnection.ParamByName('NoLogfile').Value := true;
    end
    else
    begin
      DSInsertConnection.ParamByName('NoLogfile').Value := false;
    end;
    DSInsertConnection.ExecSQL;
    ComboBox1.Items.Add(ComboBox1.Text);
  end
  else
  begin
    ShowMessage('Kein Konfigurationsname eingegeben!');
  end;
end;

procedure TForm1.UpdateDB();
var
  mySQL: string;
begin
  mySQL := 'UPDATE StoredConnections SET ConName = :ConName, Username = :Username, ' +
           '  Password = :Password, Database = :Database, Schemas = :Schemas, ' +
           '  Dumpfile = :Dumpfile, Directory = :Directory, ParJobs = :ParJobs, ' +
           '  ReuseDumpfile = :ReuseDumpfile, RemapSchema = :RemapSchema, RemapTablespace = :RemapTablespace, ' +
           '  TableExport = :TableExport, TableExportExclude = :TableExportExclude, ' +
           '  TableImport = :TableImport, TableImportExclude = :TableImportExclude, ' +
           '  Logfile = :Logfile, NoLogfile = :NoLogfile ' +
           '  WHERE ConName = :ConName';
  try
    DSUpdateConnection.CommandText := mySql;
    DSUpdateConnection.ParamByName('ConName').Value := ComboBox1.Text;
    DSUpdateConnection.ParamByName('Username').Value := edName.Text;
    DSUpdateConnection.ParamByName('Password').Value := edPassword.Text;
    DSUpdateConnection.ParamByName('Database').Value := edDatabase.Text;
    DSUpdateConnection.ParamByName('Schemas').Value := edSchema.Text;
    DSUpdateConnection.ParamByName('Dumpfile').Value := edDumpfile.Text;
    DSUpdateConnection.ParamByName('Directory').Value := edDir.Text;
    DSUpdateConnection.ParamByName('ParJobs').Value := StrToInt(edParJobs.Text);
    if cbOverwrite.Checked then
    begin
      DSUpdateConnection.ParamByName('ReuseDumpfile').Value := true;
    end
    else
    begin
      DSUpdateConnection.ParamByName('ReuseDumpfile').Value := false;
    end;
    DSUpdateConnection.ParamByName('RemapSchema').Value := edRemapSchema.Text;
    DSUpdateConnection.ParamByName('RemapTablespace').Value := edRemapTablespace.Text;
    DSUpdateConnection.ParamByName('TableExport').Value := edTableExp.Text;
    DSUpdateConnection.ParamByName('TableExportExclude').Value := edExpExclude.Text;
    DSUpdateConnection.ParamByName('TableImport').Value := edTableImp.Text;
    DSUpdateConnection.ParamByName('TableImportExclude').Value := edImpExclude.Text;
    DSUpdateConnection.ParamByName('Logfile').Value := edLogfile.Text;
    if cbNoLogfile.Checked then
    begin
      DSUpdateConnection.ParamByName('NoLogfile').Value := true;
    end
    else
    begin
      DSUpdateConnection.ParamByName('NoLogfile').Value := false;
    end;

    DSUpdateConnection.ExecSQL;
  except
      on E: EDatabaseError do
        ShowMessage('Exception raised with message' + E.Message);
  end;
end;

procedure TForm1.btExpClipboardClick(Sender: TObject);
var
  param: string;
begin
  param := '';
  if (CheckParameter()) then
  begin
    param := BuildExportString();
    DPExImportToClipboard(param, 'export');
  end;
end;

procedure TForm1.btExportClick(Sender: TObject);
var
  param: string;
begin
  param := '';
  if (CheckParameter()) then
  begin
    param := BuildExportString();
    DPExImport(param, 'export');
  end;
end;

function TForm1.BuildExportString(): string;
var
  param: string;
begin
    param := edName.Text + '/' + edPassword.Text + '@' + edDatabase.Text; //Anmeldung
    param := param + ' schemas=' + edSchema.Text; //Welche Schemata sollen exportiert werden
    param := param + ' dumpfile=' + edDumpfile.Text;
    if (edDir.Text <> '') then
    begin
      param := param + ' directory=' + edDir.Text;
    end;
    if (StrToInt(edParJobs.Text) > 0) then
    begin
      param := param + ' parallel=' + edParJobs.Text;
    end;
    if (cbOverwrite.Checked) then
    begin
      param := param + ' reuse_dumpfiles=y';
    end;
    if (edTableExp.Text <> '') then
    begin
      param := param + ' table=' + edTableExp.Text;
    end;
    if (edExpExclude.Text <> '' ) then
    begin
      param := param + ' table=' + edExpExclude.Text;
    end;
    if (cbNoLogfile.Checked = false) and
       (edLogfile.Text <> '') then
    begin
      param := param + ' logfile=' + edLogfile.Text;
    end;
    if (cbNoLogfile.Checked = true) then
    begin
      param := param + ' nologfile=y';
    end;
    result := param;
end;

procedure TForm1.btImportClick(Sender: TObject);
var
  param: string;
begin
  if (CheckParameter()) then
  begin
    param := BuildImportString();
    DPExImport(param, 'import');
  end;
end;

function TForm1.BuildImportString(): string;
var
  param: string;
begin
  param := edName.Text + '/' + edPassword.Text + '@' + edDatabase.Text; //Anmeldung
  param := param + ' schemas=' + edSchema.Text; //Welche Schemata sollen exportiert werden
  param := param + ' dumpfile=' + edDumpfile.Text;
  if (edDir.Text <> '') then
  begin
    param := param + ' directory=' + edDir.Text;
  end;
  if (StrToInt(edParJobs.Text) > 0) then
  begin
    param := param + ' parallel=' + edParJobs.Text;
  end;
  if (edRemapSchema.Text <> '') then
  begin
    param := param + ' remap_schema=' + edRemapSchema.Text;
  end;
  if (edRemapTablespace.Text <> '') then
  begin
    param := param + ' remap_tablespace=' + edRemapTablespace.Text;
  end;
  if (edTableImp.Text <> '') then
  begin
    param := param + ' table=' + edTableImp.Text;
  end;
  if (edImpExclude.Text <> '' ) then
  begin
    param := param + ' exclude=' + edImpExclude.Text;
  end;
  if (cbNoLogfile.Checked = false) and
       (edLogfile.Text <> '') then
  begin
    param := param + ' logfile=' + edLogfile.Text;
  end;
  if (cbNoLogfile.Checked = true) then
  begin
    param := param + ' nologfile=y';
  end;
  result := param;
end;

procedure TForm1.cbNoLogfileClick(Sender: TObject);
begin
  if cbNoLogfile.Checked then
    edLogfile.Enabled := false
  else
    edLogfile.Enabled := true;
end;

procedure TForm1.btInfoClick(Sender: TObject);
begin
  AboutBox.ShowModal();
end;

procedure TForm1.btLoadClick(Sender: TObject);
var
  FSQLSelectCommand: string;
begin
  if ComboBox1.Text <> '' then
  begin
    FSQLSelectCommand := 'SELECT * FROM StoredConnections where ConName=:name;';
    DSSelectConfig.CommandText := FSQLSelectCommand;
    DSSelectConfig.ParamByName('name').Value := ComboBox1.Text;
    DSSelectConfig.Open;
    DSSelectConfig.Refresh;
    edName.Text             := DSSelectConfig.Fields[2].AsString;
    edPassword.Text         := DSSelectConfig.Fields[3].AsString;
    edDatabase.Text         := DSSelectConfig.Fields[4].AsString;
    edSchema.Text           := DSSelectConfig.Fields[5].AsString;
    edDumpfile.Text         := DSSelectConfig.Fields[6].AsString;
    edDir.Text              := DSSelectConfig.Fields[7].AsString;
    edParJobs.Text          := IntToStr(DSSelectConfig.Fields[8].AsInteger);
    if DSSelectConfig.Fields[9].AsString = '1' then
    begin
      cbOverwrite.Checked := true;
    end
    else
    begin
      cbOverwrite.Checked := false;
    end;
    edRemapSchema.Text      := DSSelectConfig.Fields[10].AsString;
    edRemapTablespace.Text  := DSSelectConfig.Fields[11].AsString;
    edTableExp.Text         := DSSelectConfig.Fields[12].AsString;
    edExpExclude.Text  := DSSelectConfig.Fields[13].AsString;
    edTableImp.Text         := DSSelectConfig.Fields[14].AsString;
    edImpExclude.Text  := DSSelectConfig.Fields[15].AsString;
    edLogfile.Text          := DSSelectConfig.Fields[16].AsString;
    if DSSelectConfig.Fields[17].AsString = '1' then
    begin
      cbNoLogfile.Checked := true;
    end
    else
    begin
      cbNoLogfile.Checked := false;
    end;
  end;
end;

function TForm1.CheckParameter(): boolean;
begin
  result := false;
  if (edName.Text <> '') and
     (edPassword.Text <> '') and
     (edDatabase.Text <> '') and
     (edDumpfile.Text <> '') then
  begin
    result := true;
  end
  else
  begin
    ShowMessage('Nicht alle Pflichtparameter angegeben!');
  end;
  if ((edTableExp.Text <> '') and
     (edExpExclude.Text <> '')) or
     ((edTableImp.Text <> '') and
     (edImpExclude.Text <> '')) then
  begin
    ShowMessage('Die Parameter EXCLUDE und INCLUDE schlie�en sich gegenseitig aus!');
    result := false;
  end;
  if ((edTableExp.Text <> '') and
     (edSchema.Text <> '')) or
     ((edTableImp.Text <> '') and
     (edSchema.Text <> '')) then
  begin
    ShowMessage('Die Parameter Table und Schema schlie�en sich gegenseitig aus!');
    result := false;
  end;

end;

{
0 = The operating system is out of memory or resources.
2 = The specified file was not found
3 = The specified path was not found.
5 = Windows 95 only: The operating system denied access to the specified file
8 = Windows 95 only: There was not enough memory to complete the operation.
10 = Wrong Windows version
11 = The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)
12 = Application was designed for a different operating system
13 = Application was designed for MS-DOS 4.0
15 = Attempt to load a real-mode program
16 = Attempt to load a second instance of an application with non-readonly data segments.
19 = Attempt to load a compressed application file.
20 = Dynamic-link library (DLL) file failure.
26 = A sharing violation occurred.
27 = The filename association is incomplete or invalid.
28 = The DDE transaction could not be completed because the request timed out.
29 = The DDE transaction failed.
30 = The DDE transaction could not be completed because other DDE transactions were being processed.
31 = There is no application associated with the given filename extension.
32 = Windows 95 only: The specified dynamic-link library was not found.
}

procedure TForm1.DPExImport(param, mode: string);
var
  filename: string;
  returncode: integer;
begin
  returncode := 0;
  if (mode = 'export') then
  begin
    filename := 'expdp.exe';
  end
  else
  if (mode = 'import') then
  begin
    filename := 'impdp.exe';
  end;
  returncode := ShellExecute(handle, 'open', PChar(filename), PChar(param),'',SW_SHOWNORMAL);
  case returncode of
    0:
    begin
      ShowMessage('The operating system is out of memory or resources.');
    end;
    2:
    begin
       ShowMessage('The expdp.exe file was not found.');
    end;
  end;
end;

procedure TForm1.DPExImportToClipboard(param, mode: string);
var
  filename: string;
  command: string;
begin
  filename := '';
  command := '';
  if (mode = 'export') then
  begin
    filename := 'expdp.exe';
  end
  else
  if (mode = 'import') then
  begin
    filename := 'impdp.exe';
  end;
  command := filename + ' ' + param;
  Clipboard.Clear();
  Clipboard.AsText := command;
  ShowMessage('Kommando wurde in die Zwischenablage kopiert!');
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if FileExists('sqlite3.dll') then
  begin
    //pruefen ob DB vorhanden ist, wenn nicht -> neu anlegen
    if not FileExists('datapump.db3') then
    begin
      ShowMessage('Keine Konfigurationsdatenbank gefunden!');
      try
        SQLConnection1.Connected := false;
        ConnectToDatabase();
        CreateTable();
      except
        on E: EDatabaseError do
          ShowMessage('Exception raised with message' + E.Message);
      end;
    end
    else
    begin
      ConnectToDatabase();
    end;
    //jetzt die vorhandenen Eintraege aus der Tabelle lesen und der Combobox zuweisen
    LoadConfigFromDatabase();
  end
  else
  begin
    ShowMessage('sqlite.dll nicht gefunden. Speichern und Laden von Einstellungen nicht m�glich!');
    DisableConfig();
  end;
end;

procedure TForm1.btImpClipboardClick(Sender: TObject);
var
  param: string;
begin
  if (CheckParameter()) then
  begin
    param := BuildImportString();
    DPExImportToClipboard(param, 'import');
  end
  else
  begin
    ShowMessage('Nicht alle Pflichtparameter angegeben!');
  end;
end;

procedure TForm1.CreateTable();
var
  FSQLCommandText: string;
begin
  FSQLCommandText := 'CREATE TABLE StoredConnections (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,' +
                         'ConName VARCHAR UNIQUE, Username VARCHAR, Password VARCHAR, "Database" VARCHAR,' +
                         ' Schemas VARCHAR, Dumpfile VARCHAR, Directory VARCHAR, ParJobs INTEGER,' +
                         ' ReuseDumpfile BOOLEAN, RemapSchema VARCHAR, RemapTablespace VARCHAR,' +
                         ' TableExport VARCHAR, TableExportExclude VARCHAR, TableImport VARCHAR,' +
                         ' TableImportExclude VARCHAR, Logfile VARCHAR, NoLogfile BOOLEAN);';
  SQLConnection1.ExecuteDirect(FSQLCommandText);
end;

procedure TForm1.ConnectToDatabase();
begin
  SQLConnection1.Params.Add('Database=datapump.db3');
  SQLConnection1.Params.Add('FailIfMissing=False');
  try
    // Establish the connection.
    SQLConnection1.Connected := true;
  except
    on E: EDatabaseError do
      ShowMessage('Exception raised with message' + E.Message);
  end;
end;

procedure TForm1.LoadConfigFromDatabase();
var
  FSQLSelectCommand: string;
begin
  FSQLSelectCommand := 'SELECT * FROM StoredConnections;';
  DSSelectConnections.CommandText := FSQLSelectCommand;
  DSSelectConnections.Open();
  DSSelectConnections.First;
  while not DSSelectConnections.Eof do
  begin
    ComboBox1.Items.Add(DSSelectConnections.Fields[1].AsString);
    DSSelectConnections.Next;
  end;
  DSSelectConnections.Close();
end;

procedure TForm1.DisableConfig();
begin
  ComboBox1.Enabled := false;
  btSave.Enabled := false;
  btLoad.Enabled := false;
  btDelete.Enabled := false;
end;

end.
