object Form1: TForm1
  Left = 0
  Top = 0
  AutoSize = True
  BorderStyle = bsToolWindow
  Caption = 'Oracle DataPump'
  ClientHeight = 360
  ClientWidth = 1068
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GPAllgemein: TGroupBox
    Left = 0
    Top = 88
    Width = 353
    Height = 241
    Caption = 'Allgemeine Parameter'
    TabOrder = 1
    object Label1: TLabel
      Left = 3
      Top = 22
      Width = 47
      Height = 13
      Caption = 'Benutzer:'
    end
    object Label2: TLabel
      Left = 3
      Top = 49
      Width = 50
      Height = 13
      Caption = 'Kennwort:'
    end
    object Label3: TLabel
      Left = 3
      Top = 76
      Width = 56
      Height = 13
      Caption = 'Datenbank:'
    end
    object Label4: TLabel
      Left = 3
      Top = 103
      Width = 51
      Height = 13
      Caption = 'Schemata:'
    end
    object Label5: TLabel
      Left = 3
      Top = 130
      Width = 45
      Height = 13
      Caption = 'Dumpfile:'
    end
    object Label6: TLabel
      Left = 3
      Top = 157
      Width = 48
      Height = 13
      Caption = 'Directory:'
    end
    object Label7: TLabel
      Left = 3
      Top = 189
      Width = 69
      Height = 13
      Caption = 'Parallele Jobs:'
    end
    object Label14: TLabel
      Left = 3
      Top = 216
      Width = 35
      Height = 13
      Caption = 'Logfile:'
    end
    object edName: TEdit
      Left = 129
      Top = 19
      Width = 216
      Height = 21
      TabOrder = 0
    end
    object edPassword: TEdit
      Left = 129
      Top = 46
      Width = 216
      Height = 21
      TabOrder = 1
    end
    object edDatabase: TEdit
      Left = 129
      Top = 73
      Width = 216
      Height = 21
      TabOrder = 2
    end
    object edSchema: TEdit
      Left = 129
      Top = 100
      Width = 216
      Height = 21
      TabOrder = 3
    end
    object edDumpfile: TEdit
      Left = 129
      Top = 127
      Width = 216
      Height = 21
      TabOrder = 4
    end
    object edDir: TEdit
      Left = 129
      Top = 154
      Width = 216
      Height = 21
      TabOrder = 5
    end
    object UpDown1: TUpDown
      Left = 162
      Top = 186
      Width = 16
      Height = 21
      Associate = edParJobs
      TabOrder = 7
    end
    object edParJobs: TEdit
      Left = 129
      Top = 186
      Width = 33
      Height = 21
      TabOrder = 6
      Text = '0'
    end
    object edLogfile: TEdit
      Left = 129
      Top = 213
      Width = 216
      Height = 21
      TabOrder = 8
    end
    object cbNoLogfile: TCheckBox
      Left = 184
      Top = 188
      Width = 97
      Height = 17
      BiDiMode = bdRightToLeft
      Caption = 'Kein Logfile'
      ParentBiDiMode = False
      TabOrder = 9
      OnClick = cbNoLogfileClick
    end
  end
  object btExit: TButton
    Left = 539
    Top = 335
    Width = 82
    Height = 25
    Caption = 'Beenden'
    TabOrder = 9
    OnClick = btExitClick
  end
  object GBVoreinstellungen: TGroupBox
    Left = 0
    Top = 0
    Width = 353
    Height = 82
    Caption = 'Voreinstellungen'
    TabOrder = 0
    object ComboBox1: TComboBox
      Left = 3
      Top = 16
      Width = 342
      Height = 21
      TabOrder = 0
    end
    object btSave: TButton
      Left = 88
      Top = 43
      Width = 82
      Height = 25
      Caption = 'Speichern'
      TabOrder = 1
      OnClick = btSaveClick
    end
    object btLoad: TButton
      Left = 176
      Top = 43
      Width = 82
      Height = 25
      Caption = 'Laden'
      TabOrder = 2
      OnClick = btLoadClick
    end
    object btDelete: TButton
      Left = 264
      Top = 43
      Width = 81
      Height = 25
      Caption = 'L'#246'schen'
      TabOrder = 3
      OnClick = btDeleteClick
    end
  end
  object GPImport: TGroupBox
    Left = 715
    Top = 0
    Width = 353
    Height = 298
    Caption = 'Importeinstellungen'
    TabOrder = 3
    object Label8: TLabel
      Left = 3
      Top = 22
      Width = 77
      Height = 13
      Caption = 'Remap Schema:'
    end
    object Label9: TLabel
      Left = 3
      Top = 68
      Width = 94
      Height = 13
      Caption = 'Remap Tablespace:'
    end
    object Label12: TLabel
      Left = 3
      Top = 114
      Width = 101
      Height = 13
      Caption = 'Tabellen importieren:'
    end
    object Label13: TLabel
      Left = 3
      Top = 160
      Width = 123
      Height = 13
      Caption = 'Vom Import ausschlie'#223'en:'
    end
    object edRemapSchema: TEdit
      Left = 3
      Top = 41
      Width = 342
      Height = 21
      TabOrder = 0
    end
    object edRemapTablespace: TEdit
      Left = 3
      Top = 87
      Width = 342
      Height = 21
      TabOrder = 1
    end
    object edTableImp: TEdit
      Left = 3
      Top = 133
      Width = 342
      Height = 21
      TabOrder = 2
    end
    object edImpExclude: TEdit
      Left = 3
      Top = 179
      Width = 342
      Height = 21
      TabOrder = 3
    end
  end
  object btExport: TButton
    Left = 96
    Top = 335
    Width = 82
    Height = 25
    Caption = 'Export'
    TabOrder = 5
    OnClick = btExportClick
  end
  object btImport: TButton
    Left = 316
    Top = 335
    Width = 82
    Height = 25
    Caption = 'Import'
    TabOrder = 7
    OnClick = btImportClick
  end
  object btInfo: TButton
    Left = 8
    Top = 335
    Width = 82
    Height = 25
    Caption = 'Info'
    TabOrder = 4
    OnClick = btInfoClick
  end
  object GPExport: TGroupBox
    Left = 359
    Top = 0
    Width = 353
    Height = 298
    Caption = 'Exporteinstellungen'
    TabOrder = 2
    object Label10: TLabel
      Left = 3
      Top = 22
      Width = 103
      Height = 13
      Caption = 'Tabellen exportieren:'
    end
    object Label11: TLabel
      Left = 3
      Top = 68
      Width = 123
      Height = 13
      Caption = 'Vom Export ausschlie'#223'en:'
    end
    object cbOverwrite: TCheckBox
      Left = 3
      Top = 114
      Width = 141
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Dumpfiles '#252'berschreiben:'
      TabOrder = 2
    end
    object edTableExp: TEdit
      Left = 3
      Top = 41
      Width = 347
      Height = 21
      TabOrder = 0
    end
    object edExpExclude: TEdit
      Left = 3
      Top = 87
      Width = 347
      Height = 21
      TabOrder = 1
    end
  end
  object btExpClipboard: TButton
    Left = 184
    Top = 335
    Width = 126
    Height = 25
    Caption = 'Exp in Zwischenablage'
    TabOrder = 6
    OnClick = btExpClipboardClick
  end
  object btImpClipboard: TButton
    Left = 404
    Top = 335
    Width = 129
    Height = 25
    Caption = 'Imp in Zwischenablage'
    TabOrder = 8
    OnClick = btImpClipboardClick
  end
  object SQLConnection1: TSQLConnection
    ConnectionName = 'SQLConnect'
    DriverName = 'Sqlite'
    LoginPrompt = False
    Params.Strings = (
      'DriverUnit=Data.DbxSqlite'
      
        'DriverPackageLoader=TDBXSqliteDriverLoader,DBXSqliteDriver250.bp' +
        'l'
      
        'MetaDataPackageLoader=TDBXSqliteMetaDataCommandFactory,DbxSqlite' +
        'Driver250.bpl'
      'FailIfMissing='
      'Database=')
    Left = 81
    Top = 104
  end
  object DSInsertConnection: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 16
    Top = 40
  end
  object DSSelectConnections: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 480
    Top = 216
  end
  object DSSelectConfig: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 80
    Top = 152
  end
  object DSUpdateConnection: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 392
    Top = 192
  end
  object DSDeleteConnection: TSQLDataSet
    MaxBlobSize = -1
    Params = <>
    SQLConnection = SQLConnection1
    Left = 536
    Top = 176
  end
end
