﻿{
This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
}
unit About;

interface

uses
WinApi.Windows,
System.SysUtils,
System.Classes,
Vcl.Graphics,
Vcl.Forms,
Vcl.Controls,
Vcl.StdCtrls,
Vcl.Buttons,
Vcl.ExtCtrls,
JclFileUtils, Vcl.Dialogs, JvBaseDlg, JvJVCLAboutForm, Vcl.Imaging.pngimage;

type
  TAboutBox = class(TForm)
    Panel1: TPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    OKButton: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private-Deklarationen }
    function GetProductVersion(FileName: string): string;
    function GetCopyright(FileName: string): string;
    function GetOriginalFileName(FileName: string): string;
  public
    { Public-Deklarationen }
  end;

var
  AboutBox: TAboutBox;

implementation

{$R *.dfm}

(*******************************************************************************
 Method       : FormCreate
 Information  : Methode die bei Erzeugung des Form aufgerufen wird. Hier wird
                der Dateiname ermittelt und im Form angezeigt.
                                                                              *)
procedure TAboutBox.FormCreate(Sender: TObject);
var
  appname: string;
begin
  appname := ExtractFileName(Application.ExeName);
  Label1.Caption := GetProductVersion(appname);
  Label2.Caption := GetCopyright(appname);
  Label4.Caption := GetOriginalFileName(appname);
end;

function TAboutBox.GetProductVersion(FileName: string): string;
var fi: TJclFileVersionInfo;
begin
  fi:=TJclFileVersionInfo.Create(FileName);
  try
    result := fi.ProductVersion;
  finally
    fi.free;
  end;
end;

function TAboutBox.GetCopyright(FileName: string): string;
var fi: TJclFileVersionInfo;
begin
  fi := TJclFileVersionInfo.Create(FileName);
  try
    result := fi.LegalCopyright;
  finally
    fi.Free;
  end;
end;

function TAboutBox.GetOriginalFileName(FileName: string): string;
var fi: TJclFileVersionInfo;
begin
  fi := TJclFileVersionInfo.Create(FileName);
  try
    result := fi.OriginalFilename;
  finally
    fi.Free;
  end;
end;

end.
 
