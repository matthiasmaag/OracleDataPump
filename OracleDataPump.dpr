program OracleDataPump;

uses
  Vcl.Forms,
  OracleDataPumpMain in 'OracleDataPumpMain.pas' {Form1},
  ABOUT in 'ABOUT.pas' {AboutBox},
  Vcl.Themes,
  Vcl.Styles;

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.Run;
end.
